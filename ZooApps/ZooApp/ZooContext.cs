﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ZooApp
{
    class ZooContext : DbContext
    {
        public ZooContext() :base ("ZooContext")
        {

        }
        public DbSet<Animals> Animals { get; set; }
    }
}
