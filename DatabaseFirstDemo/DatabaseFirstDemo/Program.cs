﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseFirstDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new DatabaseFirstDemoEntities1();
            var table = new Table_1()
            {
                   Body="Body",
                   DataPublished=DateTime.Now,
                   Title="Title",
                   PostID= 1
            };
            context.Table_1.Add(table);
            context.SaveChanges();
        }
    }
}
