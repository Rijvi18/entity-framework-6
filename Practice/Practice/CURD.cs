﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{

    class CURD
    {
        public static void Main(string[] args)
        {
            MyDBEntities db = new MyDBEntities();
            Area areaObj = new Area();

            int opt,id;
            do
            {
                Console.WriteLine("1:New Record\n2:Display\n3:Update Record\n4:Delete Record\n5:Exit");
                Console.WriteLine("Enter your option");
                opt = Convert.ToInt32(Console.ReadLine());

                switch (opt)
                {
                    case 1://add new record
                        Console.WriteLine("Enter AreaName,CityId,PinCode:");
                        areaObj.AreaName = Console.ReadLine();
                        areaObj.CityId = Convert.ToInt32(Console.ReadLine());
                        areaObj.PinCode = Console.ReadLine();

                        db.Areas.Add(areaObj);
                        db.SaveChanges();
                        break;

                    case 2://read all record
                        foreach (Area a in db.Areas)
                        {
                            Console.WriteLine("{0}\t{1}\t{2}\t{3}", a.AreaId, a.AreaName, a.PinCode,a.City.CityNmae);
                        }
                        break;
                    case 3://Update record
                        Console.WriteLine("Enter ID to Update");
                        id = Convert.ToInt32(Console.ReadLine());
                        areaObj = db.Areas.Find(id);
                        if (areaObj == null)
                        {
                            Console.WriteLine("Invalid Id, Try again");

                        }
                        else
                        {
                            Console.WriteLine("Enter Modified AreaName,CityId,PinCode:");
                            areaObj.AreaName = Console.ReadLine();
                            areaObj.CityId = Convert.ToInt32(Console.ReadLine());
                            areaObj.PinCode = Console.ReadLine();
                            db.SaveChanges();
                        }

                        break;
                    case 4://delete record
                        Console.WriteLine("Enter ID to Delete");
                        id = Convert.ToInt32(Console.ReadLine());
                        areaObj = db.Areas.Find(id);
                        if (areaObj == null)
                        {
                            Console.WriteLine("Invalid Id, Try again");

                        }
                        else
                        {
                            db.Areas.Remove(areaObj);
                            db.SaveChanges();
                        }

                         break;
                    case 5: break;
                    default: Console.WriteLine("Invalid Option"); break;


                }

            } while (opt != 5);
        }
    }
}
