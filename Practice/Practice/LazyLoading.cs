﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class LazyLoading
    {
        public static void Main(string[] args)
        {
            MyDBEntities db = new MyDBEntities();
             foreach(City cobj in db.Cities)
            {
                Console.WriteLine("{0}\t{1}", cobj.CityId, cobj.CityNmae);
                foreach(Area aobj in cobj.Areas)
                {
                    Console.WriteLine(aobj.AreaName);
                }
            }
        }
    }
}
