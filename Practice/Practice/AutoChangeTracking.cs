﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class AutoChangeTracking
    {
        public static void Main(string[] args)
        {
            MyDBEntities db = new MyDBEntities();
            Area newArea = new Area()
            {
                AreaName =  "Balubagan",CityId=3,PinCode="6544"
            };
            db.Areas.Add(newArea);

            Console.WriteLine("After Adding");
            foreach(var tracker in db.ChangeTracker.Entries<Area>())
            {
                Console.WriteLine(tracker.State);
            }
            Area modifiedArea = db.Areas.Find(5);
            if (modifiedArea != null)
            {
                modifiedArea.PinCode = "6544";
            }
            Console.WriteLine("After Modifying");
            foreach(var tracker in db.ChangeTracker.Entries<Area>())
            {
                Console.WriteLine(tracker.State);
            }
            Area delArea = db.Areas.Find(3);
            if (delArea != null)
            {
                db.Areas.Remove(delArea);
            }
            Console.WriteLine("After removal");
            foreach(var tracker in db.ChangeTracker.Entries<Area>())
            {
                Console.WriteLine(tracker.State);
            }
        }
    }
}
